/*
 This file is part of rdnssd_osx
 OSX port (C) 2011 Tony Hoyle <tony@hoyle.me.uk>
 Based largely on rdnssd-win32
 Copyright (C) 2008 Sebastien Vincent <sebastien.vincent@cppextrem.com>
 
 rdnssd_osx is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 rdnssd_osx is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with rdnssd_osx.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file rdnssd.c
 * \brief Recursive DNS Server daemon for OSX.
 * \author Tony Hoyle
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <signal.h>

#include <utime.h>
#include <sys/types.h>

#include <errno.h>

#include <unistd.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <pcap.h>

#include "packet.h"

int debug;

/**
 * \def IFNAME_MAX
 * \brief Max. Length of interface name
 */
#define IFNAME_MAX 128 

/**
 * \def PACKET_CAPTURE_LEN
 * \brief Captured packet len.
 */
#define PACKET_CAPTURE_LEN 1514

/**
 * \def PACKET_TIMEOUT
 * \brief Timeout (for pcap_open_live).
 */
#define PACKET_TIMEOUT 100

/**
 * \def MAX_RDNSS
 * \brief Max number of RDNSS option.
 */
#define MAX_RDNSS 16

/**
 * \struct rdnss_t
 * \brief Server information.
 */
typedef struct rdnss_t
{
    struct in6_addr addr; /**< IPv6 address of the server */
    unsigned int    ifindex; /**< interface index */
    time_t          expiry; /**< expire time */
} rdnss_t;

/**
 * \struct rdnss_servers
 * \brief The list of dns servers.
 */
typedef struct rdnss_servers
{
    size_t  count; /**< Number of servers */
    rdnss_t list[MAX_RDNSS]; /**< Array of server information */
}rdnss_servers;

/**
 * \var now
 * \brief The current time.
 */
static time_t now;

/**
 * \var ifname
 * \brief Interface name (i.e {xxx-xxx-xxx...}).
 */
static char ifname[IFNAME_MAX];

/**
 * \var sock
 * \brief the interface sniffer.
 * Must be global if we want to cleanup when pcap_breakloop.
 */
static pcap_t* sock=NULL;

/**
 * \var servers
 * \brief DNS list information.
 */
static struct rdnss_servers servers;

/* windows specific code */

/**
 * \enum clockid_t
 * \brief Different type of clock (used with clock_* function).
 */
typedef enum clockid_t
{
    CLOCK_REALTIME, /**< The realtime clock */
    CLOCK_MONOTONIC /**< The monotonic clock */
}clockid_t;

/**
 * \brief A clock_gettime function replacement.
 * \param clk_id the type of clock we want
 * \param tp structure that will be filled with the time
 * \return 0 if success, negative integer otherwise
 * \author Sebastien Vincent
 */
static int clock_gettime(clockid_t clk_id, struct timespec *tp)
{
    struct timeval tv;
	
    if (gettimeofday(&tv, NULL)==-1)
    {
        return -1;
    }
	
    tp->tv_sec=tv.tv_sec;
    tp->tv_nsec=tv.tv_usec * 1000; /* convert microsecond to nanosecond */
    return 0;
}

static char ServiceGuid[38];

static void get_old_server_list(char *buf, size_t buflen)
{
	/* SERVICE_GUID=`echo show State:/Network/Global/IPv6 | scutil | awk '/PrimaryService/ {print $3}'`
	 echo "show State:/Network/Service/$SERVICE_GUID/DNS ServerAddresses" | scutil | awk '/[0-9]+ : / {print $3}' */
	
    FILE *f;
    char *p,*q;
    int state;
	
    ServiceGuid[0]='\0';
    f = popen("scutil","r+");
    if(!f)
    {
		fprintf(stderr,"couldn't open scutil\n");
		return;
    }
    fprintf(f,"show State:/Network/Global/IPv6\n");
    while(!feof(f))
    {
		fgets(buf,buflen,f);
		if(strstr(buf,"PrimaryService"))
		{
			p=buf;
			while(*p && *p!=':') p++;
			if(*p)
			{
				p+=2;
				memcpy(ServiceGuid, p, 36);
				ServiceGuid[37]='\0';
				break;
			}
		}
    }
    pclose(f);
	
    if(!ServiceGuid[0])
    {
		fprintf(stderr,"couldn't get service guid");
		return;
    }
	
    f = popen("scutil","r+");
    if(!f)
    {
        fprintf(stderr,"couldn't open scutil");
        return;
    }
    fprintf(f,"show State:/Network/Service/%s/DNS\n", ServiceGuid);
    state=1;
    while(state!=3 && buflen>8 && !feof(f))
    {
		fgets(buf,buflen,f);
		if(state==1 && strstr(buf,"ServerAddresses")) state=2;
		else if(state==2 && strchr(buf,'}')) state=3;
		else if(state==2)
		{
			p=buf;
			while(*p && *p!=':') p++;
			if(*p)
			{
				p+=2;
				q=buf;
				while(*p && *p!=' ' && *p!='\n') *(q++)=*(p++);
				*(q++)='\n';
				buflen -= (q-buf);
				buf=q;
			}
		}
    }
    *buf='\0';
    pclose(f);
}

static void write_new_server_list(char *buf)
{
    FILE *f;
    char *p;
	
    f = popen("scutil","r+");
    if(!f)
    {
        fprintf(stderr,"couldn't open scutil\n");
        return;
    }
    fprintf(f,"get State:/Network/Service/%s/DNS\n", ServiceGuid);
    
    while(*buf)
    {
		p=buf;
		while(*p && *p!='\n') p++;
		*(p++)='\0';
		fprintf(f,"d.add ServerAddresses * %s\n",buf); 
		buf=p;
    }
	
    fprintf(f,"set State:/Network/Service/%s/DNS\n", ServiceGuid);
    pclose(f);
}


/**n
 * \brief Write name servers 
 */
void rdnssd_write(void)
{
    char old[10240];
    char buf[10240];
    char *buf2;
    struct rdnss_t* rd=NULL;
    size_t bufsize;
    size_t i=0;
	
    if (!servers.count)
    {
        return;
    }
	
    get_old_server_list(old, sizeof(old));
    if(debug) printf("Old list is %s",old);
	
    buf2=buf;
    bufsize=sizeof(buf);
	
    for (i=0;i<servers.count;i++)
    {
        rd=&servers.list[i];
		
		if(bufsize<INET6_ADDRSTRLEN) break;
		
        inet_ntop(AF_INET6, &rd->addr, buf2, INET6_ADDRSTRLEN);
		
        bufsize-=strlen(buf2);
        buf2+=strlen(buf2);
		*(buf2++)='\n';
		bufsize--;
    }
    *buf2='\0';
    if(debug) printf("New value is %s", buf);
    
    bufsize=strlen(buf);
	
    if (!strcmp(buf, old))
    {
        if(debug) printf("Same value, don't update\n");
        return;
    }
	
    write_new_server_list(buf);
	
    if(debug) printf("DNS server(s) written\n");
}

/**
 * \brief Remote a entry in the table if lifetime is expired.
 * \author Pierre Ynard
 */
void rdnssd_trim_expired (void)
{
    while (servers.count > 0
		   && servers.list[servers.count - 1].expiry <= now)
        servers.count--;
}

/**
 * \brief Compare function.
 * \param a first parameter
 * \param b second parameter
 * \return 0 if equal, 1 if "a" is lower than "b", -1 if "a" is greater than than "b"
 * \author Pierre Ynard
 */
static int rdnssd_is_older (const void *a, const void *b)
{
    time_t ta = ((const rdnss_t *)a)->expiry;
    time_t tb = ((const rdnss_t *)b)->expiry;
	
    if (ta < tb)
        return 1;
    if (ta > tb)
        return -1;
    return 0;
}

/**
 * \brief Update the name servers list.
 * \param addr IPv6 address of the name server
 * \param ifindex interface index on which we receive the RA
 * \param expiry lifetime of the entry
 * \author Pierre Ynard
 */
static void rdnssd_update(struct in6_addr* addr, unsigned int ifindex, time_t expiry)
{
    size_t i=0;
	
    /* Does this entry already exist? */
    for (i = 0; i < servers.count; i++)
    {
        if (memcmp (addr, &servers.list[i].addr, sizeof (*addr)) == 0
			&& (! IN6_IS_ADDR_LINKLOCAL(addr)
				|| ifindex == servers.list[i].ifindex))
            break;
    }
	
    /* Add a new entry */
    if (i == servers.count)
    {
        if (expiry == now)
            return; /* Do not add already expired entry! */
		
        if (servers.count < MAX_RDNSS)
            i = servers.count++;
        else
        {
            /* No more room? replace the most obsolete entry */
            if ((expiry - servers.list[MAX_RDNSS - 1].expiry) >= 0)
                i = MAX_RDNSS - 1;
        }
    }
	
    memcpy (&servers.list[i].addr, addr, sizeof (*addr));
    servers.list[i].ifindex = ifindex;
    servers.list[i].expiry = expiry;
	
    qsort (servers.list, servers.count, sizeof (rdnss_t), rdnssd_is_older);
}

/**
 * \brief Parse the Neighbor Discovery options, searching the RDNSS option.
 * \param opt pointer on the options
 * \param opts_len length of the options
 * \param ifindex interface index
 * \return 0 if success, -1 otherwise
 * \author Pierre Ynard
 * \author Sebastien Vincent
 */
int rdnssd_parse_nd_opts (const struct nd_opt_hdr *opt, size_t opts_len, unsigned int ifindex)
{
    struct in6_addr *addr=NULL;
	
    for (; opts_len >= sizeof(struct nd_opt_hdr);opts_len -= opt->nd_opt_len << 3,
		 opt = (const struct nd_opt_hdr *)((const uint8_t *) opt + (opt->nd_opt_len << 3)))
    {
        struct nd_opt_rdnss *rdnss_opt=NULL;
        size_t nd_opt_len = opt->nd_opt_len;
        uint32_t lifetime=0;
		
        if (nd_opt_len == 0 || opts_len < (nd_opt_len << 3))
            return -1;
		
        if (opt->nd_opt_type != ND_OPT_RDNSS)
            continue;
		
        if (nd_opt_len < 3 /* too short per RFC */
			|| (nd_opt_len & 1) == 0) /* bad (even) length */
            continue;
		
        rdnss_opt = (struct nd_opt_rdnss *) opt;
		
        if(debug) printf("rdnss option found!\n");
		
        {
            struct timespec ts;
            clock_gettime(CLOCK_MONOTONIC, &ts);
            now = ts.tv_sec;
        }
		
        lifetime = (uint32_t)now + ntohl(rdnss_opt->nd_opt_rdnss_lifetime);
		
        for (addr = (struct in6_addr *)(rdnss_opt + 1); nd_opt_len >= 2; addr++, nd_opt_len -= 2)
        {
            rdnssd_update(addr, ifindex, lifetime);
        }
    }
	
    return 0;
}

/**
 * \brief Callback for the frame analyze.
 * \param args not used
 * \param header packet information (size, ...)
 * \param packet the packet
 * \return 0
 */
void rdnssd_decode_frame(u_char* args, const struct pcap_pkthdr* header, const u_char* packet)
{
	if(debug) printf("Received icmpv6 packet of %d bytes\n", header->len);
    if (packet_decode_ethernet(packet, header->len)==1)
    {
		if(debug) printf("RA packet decode successful\n");
		
        /* if returns 1, the packet is a RA */
        rdnssd_trim_expired();
        /* write nameservers */
        rdnssd_write();
    }
}

/**
 * \brief Function executed when the program receive a signal.
 * \param code code of the signal
 */
static void signal_routine(int code)
{
    switch (code)
    {
		case SIGTERM:
		case SIGINT:
		case SIGABRT:
			printf("Terminating program\n");
			if (sock)
			{
				pcap_breakloop(sock);
			}
			break;
		case SIGSEGV:
			printf("Receive SIGSEGV: oups, exiting now\n");
			exit(EXIT_FAILURE); /* we just exit the program without cleanup */
			break;
		default:
			break;
    }
}

/**
 * \brief rdnssd program.
 * \param argc number of argument
 * \param argv array of argument
 * \return EXIT_SUCCESS or EXIT_FAILURE
 */
static int rdnssd_main(int argc, char** argv)
{
    /* signals handling */
    if (signal(SIGTERM, signal_routine) == SIG_ERR)
    {
        printf("SIGTERM not handled\n");
    }
	
    if (signal(SIGINT, signal_routine) == SIG_ERR)
    {
        printf("SIGINT not handled\n");
    }
	
    if (signal(SIGABRT, signal_routine) == SIG_ERR)
    {
        printf("SIGABRT not handled\n");
    }
	
    if (signal(SIGSEGV, signal_routine) == SIG_ERR)
    {
        printf("SIGSEGV not handled\n");
    }
	
    /* capture the packet */
    pcap_loop(sock, -1, rdnssd_decode_frame, NULL);
	
    pcap_close(sock);
	
    return EXIT_SUCCESS;
}

void usage(const char *app)
{
	pcap_if_t * devices=NULL;
	pcap_if_t* ifdev=NULL;
	char error[PCAP_ERRBUF_SIZE];
	
	printf("Usage: %s [-d] ifname\n", app);
	
	if (pcap_findalldevs(&devices, error)==-1)
	{
		printf("Error: %s\n", error);
		exit(EXIT_FAILURE);
	}
	
	printf("Available interfaces: \n");
	for (ifdev=devices;ifdev;ifdev=ifdev->next)
	{
		printf("\t%s\n", ifdev->name);
	}
	pcap_freealldevs(devices);
	exit(EXIT_FAILURE);
}

/**
 * \brief Entry point of the program.
 * \param argc number of arguments
 * \param argv array of arguments
 * \return EXIT_SUCCESS or EXIT_FAILURE
 */
int main(int argc, char** argv)
{
    struct bpf_program bpf;
    char* filter="icmp6";
    char* dev=NULL;
    char error[PCAP_ERRBUF_SIZE];
	char *app = argv[0];
	int c;
	int background = 0;
	
    /* init list */
    memset(&servers, 0x00, sizeof(servers));
    servers.count=0;
	debug = 0;
	
	while((c=getopt(argc, argv, "db"))!=-1)
	{
		switch(c)
		{
			case 'd':  debug = 1; break;
			case 'b':  background = 1; break;
			case '?':
			default:
				usage(app);
				break;
		}
	}
	argc -= optind;
	argv += optind;
	
    if (argc!=1)
		usage(app);
	
	dev=argv[0];
	
    /* copy the interface name */
    strncpy(ifname, dev, IFNAME_MAX-1);
    ifname[IFNAME_MAX-1]=0x00;
	
    if(debug) printf("Listening on %s\n", ifname);
    sock=pcap_open_live(dev, PACKET_CAPTURE_LEN, 0, PACKET_TIMEOUT, error);
	
    if (!sock)
    {
        printf("%s\n", error);
        exit(EXIT_FAILURE);
    }
	
    /* compile filter */
    if (pcap_compile(sock, &bpf, filter, 0, 0)==-1)
    {
        printf("Error pcap_compile\n");
        pcap_close(sock);
        exit(EXIT_FAILURE);
    }
	
    /* set filter */
    if (pcap_setfilter(sock, &bpf)==-1)
    {
        printf("Error pcap_setfilter\n");
        pcap_freecode(&bpf);
        pcap_close(sock);
        exit(EXIT_FAILURE);
    }
	
    pcap_freecode(&bpf);
	
	if(background) if(daemon(0,0)<0) { perror("couldn't background"); return EXIT_FAILURE; }
	
    rdnssd_main(argc, argv);
	
    return EXIT_SUCCESS;
}
