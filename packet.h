/*
    This file is part of rdnssd_osx
    OSX port (C) 2011 Tony Hoyle <tony@hoyle.me.uk>
    Based largely on rdnssd-win32
    Copyright (C) 2008 Sebastien Vincent <sebastien.vincent@cppextrem.com>

    rdnssd_osx is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    rdnssd_osx is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with rdnssd_osx.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * \file packet.h
 * \brief Ethernet, IPv6 and ICMPv6 headers.
 * \author Sebastien Vincent
 */

#ifndef PACKET_H
#define PACKET_H

/**
 * \def ND_OPT_RDNSS
 * \brief Option type number for RDNSS option.
 */
#define ND_OPT_RDNSS 25

#define PACKED __attribute__((packed))

/**
 * \struct eth_hdr
 * \brief Ethernet protocol header 
 */
typedef struct eth_hdr
{
    uint8_t dst_addr[6]; /**< destination address */
    uint8_t src_addr[6]; /**< source address */
    uint16_t ether_type;  /**< Ethernet type of the payload */
} PACKED eth_hdr;

/**
 * \struct ipv6_hdr
 * \brief IPv6 protocol header
 */
typedef struct ipv6_hdr
{
    uint32_t   ipv6_vertcflow;        /**< 4 bit IPv6 version\n
                                           8 bit traffic class\n
                                           20 bit flow label */
    uint16_t  ipv6_payloadlen;       /**< payload length */
    uint8_t   ipv6_nexthdr;          /**< next header protocol value */
    uint8_t   ipv6_hoplimit;         /**< TTL */
    struct in6_addr ipv6_srcaddr;          /**< source address */
    struct in6_addr ipv6_destaddr;         /**< destination address */
} PACKED ipv6_hdr;

/**
 * \struct ipv6_fragment_hdr
 * \brief IPv6 fragment header
 */
typedef struct ipv6_fragment_hdr
{
    uint8_t   ipv6_frag_nexthdr; /**< next header protocol value */
    uint8_t   ipv6_frag_reserved; /**< reserved */
    uint16_t  ipv6_frag_offset; /**< offset */
    uint32_t   ipv6_frag_id; /**< Id of the fragment */
} PACKED ipv6_fragment_hdr;

/**
 * \struct icmpv6_hdr
 * \brief ICMPv6 header
 */
typedef struct icmpv6_hdr
{
    uint8_t   icmp6_type; /**< ICMPv6 type */
    uint8_t   icmp6_code; /**< ICMPv6 code */
    uint16_t  icmp6_checksum; /**< ICMPv6 checksum */
} PACKED icmpv6_hdr;

/**
 * \struct nd_router_advert
 * \brief Router Advertisement message.
 */
struct nd_router_advert
{
    struct icmpv6_hdr      nd_ra_hdr; /**< ICMPv6 header */
    uint32_t               nd_ra_reachable;        /**< reachable time */
    uint32_t               nd_ra_retransmit;       /**< retransmit timer */
    /* could be followed by options */
} PACKED nd_router_advert;

/**
 * \struct nd_opt_hdr
 * \brief ICMPv6 option header.
 */
typedef struct nd_opt_hdr
{
    uint8_t            nd_opt_type; /**< ICMPv6 option type */
    uint8_t            nd_opt_len; /**< length of the option (multiple of 8 bytes) */
} PACKED nd_opt_hdr;

/**
 * \struct nd_opt_rdnss
 * \brief ICMPv6 RDNSS option header.
 */
struct nd_opt_rdnss
{
    uint8_t nd_opt_rdnss_type; /**< ICMPv6 RDNSS option type = 25 */
    uint8_t nd_opt_rdnss_len; /**< length of the option (multiple of 8 bytes) */
    uint16_t nd_opt_rdnss_resserved1; /**< reserved value */
    uint32_t nd_opt_rdnss_lifetime; /**< lifetime of the entry */
    /* followed by one or more IPv6 addresses */
} PACKED nd_opt_rdnss;

/**
 * \brief Decode an ethernet frame.
 * \param packet the packet
 * \param len length of the packet
 * \return 0 if success, -1 otherwise
 * \note It returns 1 in case the packet is a Router Advertisement.
 */
int packet_decode_ethernet(const u_char* packet, size_t len);

/**
 * \brief Decode an IPv6 packet.
 * \param packet the packet
 * \param len length of the packet
 * \return 0 if success, -1 otherwise
 * \note It returns 1 in case the packet is a Router Advertisement.
 */
int packet_decode_ipv6(const u_char* packet, size_t len);

/**
 * \brief Decode an ICMPv6 packet.
 * \param packet the packet
 * \param len length of the packet
 * \return 0 if success, -1 otherwise
 * \note It returns 1 in case the packet is a Router Advertisement.
 */
int packet_decode_icmpv6(const u_char* packet, size_t len);

#endif /* IP6HDR_H */
